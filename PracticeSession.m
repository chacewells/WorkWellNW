//
//  PracticeSession.m
//  WorkWellNW
//
//  Created by Aaron Wells on 2/2/15.
//  Copyright (c) 2015 Aaron Wells. All rights reserved.
//

#import "PracticeSession.h"


@implementation PracticeSession

@dynamic date;
@dynamic duration;
@dynamic guided;

@end
