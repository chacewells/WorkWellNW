//
//  GuidedMeditation.h
//  WorkWellNW
//
//  Created by Aaron Wells on 2/2/15.
//  Copyright (c) 2015 Aaron Wells. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface GuidedMeditation : NSManagedObject

@property (nonatomic, retain) NSData * data;
@property (nonatomic, retain) NSString * title;

@end
