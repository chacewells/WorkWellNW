//
//  PracticeSessions.h
//  YogaApp
//
//  Created by Aaron Wells on 10/23/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface PracticeSessions : NSObject
+ (NSArray *)allPracticeSessions;
+ (NSArray *)syncPracticeSessions;
+ (NSDictionary *)totalTimePracticed;
+ (NSInteger)weeksSinceFirstPracticeSession;
+ (NSInteger)count;

@end
