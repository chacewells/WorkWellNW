//
//  CoreDataTVCTableViewController.h
//  YogaApp
//
//  Created by Aaron Wells on 10/18/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"

@interface CoreDataTVC : UITableViewController <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *frc;

- (void)performFetch;
@end
