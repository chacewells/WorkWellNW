//
//  MindfulMinuteVC.h
//  YogaApp
//
//  Created by Aaron Wells on 10/19/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import <UIKit/UIKit.h>
@class MindfulMinuteInstance, MindfulMinuteTemplate, CoreDateHelper;
@import CoreData;

@interface MindfulMinuteVC : UIViewController

@property (strong, nonatomic) NSManagedObjectID *selectedMindfulMinuteInstanceID;

@end
