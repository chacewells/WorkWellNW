//
//  NotificationHelper.h
//  YogaApp
//
//  Created by Aaron Wells on 10/19/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import <Foundation/Foundation.h>
@import CoreData;

@class CoreDataHelper;

@protocol NotificationHelperDelegate <NSObject>
- (CoreDataHelper *)cdh;

@optional

@end

@interface NotificationHelper : NSObject
@property(weak, nonatomic) id <NotificationHelperDelegate> delegate;

- (void)updateNotifications;
- (NSArray*)allMindfulMinuteInstances;
- (NSArray*)allMindfulMinuteTemplates;
- (NSArray*)allLocalNotifications;
- (NSMutableArray*)mutableArrayOfMutableArraysWithMindfulMinuteInstanceAsFirstElement:(NSArray*)mmis;
- (void)addNotifications:(NSArray*)notifications toCorrespondingMindfulMinuteInstanceArrays:(NSMutableArray*)mmArray;
- (void)scheduleNotificationsInSubarrays:(NSMutableArray*)mmArray withMindfulMinuteTemplates:(NSArray*)mmts forTotalAllowableNotifications:(int)totalAllowableNotificationsCount;
- (void)showScheduledNotifications;

@end
