//
//  MindfulMinuteVC.m
//  YogaApp
//
//  Created by Aaron Wells on 10/19/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "MindfulMinuteVC.h"
#import "AppDelegate.h"
#import "CoreDataHelper.h"
#import "MindfulMinuteInstance.h"
#import "DateUtils.h"

@interface MindfulMinuteVC ()
@property (weak, nonatomic) IBOutlet UIDatePicker *picker;

- (IBAction)finished:(id)sender;
- (IBAction)updateMmi:(id)sender;
@end

@implementation MindfulMinuteVC
#define debug 1

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self updateTimePicker];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - INTERACTION
- (IBAction)finished:(id)sender {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    
//    for testing purposes
//    UILocalNotification *notify = [[UILocalNotification alloc] init];
//    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute fromDate:self.picker.date];
//    NSDate *startOfMinute = [[NSCalendar currentCalendar] dateFromComponents:components];
//    notify.fireDate = startOfMinute;
//    notify.alertBody = @"Calm down. The app will work just fine.";
//    notify.soundName = @"klang.mp3";
//    [[UIApplication sharedApplication] scheduleLocalNotification:notify];
//    NSLog(@"Notification supposedly scheduled for %@", [NSDateFormatter localizedStringFromDate:startOfMinute dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterShortStyle]);
    
    [[(AppDelegate*)[[UIApplication sharedApplication] delegate] nh] updateNotifications];
    [self.navigationController popViewControllerAnimated:YES];
}

//  gets time of day from timePicker and adds to current MindfulMinuteInstance
- (IBAction)updateMmi:(id)sender {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    
    CoreDataHelper *cdh = [(AppDelegate*)[[UIApplication sharedApplication] delegate] cdh];
    
    NSError *error = nil;
    MindfulMinuteInstance *mmi = (MindfulMinuteInstance*)[cdh.context existingObjectWithID:self.selectedMindfulMinuteInstanceID error:&error];
    if (error) {NSLog(@"%@", error);}
    
    NSTimeInterval interval = [DateUtils timeIntervalFromHoursAndMinutesOfDate:self.picker.date];
    if (debug==1) NSLog(@"TimeInterval: %f", interval);
    
    mmi.timeOfDay = [NSNumber numberWithDouble:interval];
    if (debug == 1) {
        NSLog(@"%@ timeOfDay: %@", @"updateMmi", mmi.timeOfDay);
        NSLog(@"timeOfDay: %@", [NSDateFormatter localizedStringFromDate:self.picker.date dateStyle:0 timeStyle:NSDateFormatterShortStyle]);
    }
    [cdh saveContext];
}

#pragma mark - VIEW
- (void)updateTimePicker {
    NSDate *theDate = [DateUtils startOfDayWithDate:[NSDate date]];
    CoreDataHelper *cdh = [(AppDelegate*)[[UIApplication sharedApplication] delegate] cdh];
    NSError *error = nil;
    MindfulMinuteInstance *mmi = (MindfulMinuteInstance*)[cdh.context existingObjectWithID:self.selectedMindfulMinuteInstanceID error:&error];
    
    if (error){NSLog(@"Error %@: %@", error, error.localizedDescription);}
    if (debug==1) {NSLog(@"%@: (before) mmi.timeOfDay: %@", NSStringFromSelector(_cmd), mmi.timeOfDay);}
    
    if ([mmi.timeOfDay isEqualToNumber:[NSNumber numberWithInt:0]]) {
        theDate = [NSDate date];
        mmi.timeOfDay = [NSNumber numberWithDouble:[DateUtils timeIntervalFromHoursAndMinutesOfDate:theDate]];
    } else {
        NSTimeInterval instanceTime = [mmi.timeOfDay doubleValue];
        theDate = [theDate dateByAddingTimeInterval:instanceTime];
    }
    
    self.picker.date = theDate;
    [cdh saveContext];
}

@end
