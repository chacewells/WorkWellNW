//
//  AppDelegate.h
//  YogaApp
//
//  Created by Aaron Wells on 10/18/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CoreDataHelper.h"
#import "NotificationHelper.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate, NotificationHelperDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (nonatomic, strong, readonly) CoreDataHelper *coreDataHelper;
@property (strong, nonatomic) NotificationHelper *notificationHelper;

- (CoreDataHelper*)cdh;
- (NotificationHelper*)nh;
@end