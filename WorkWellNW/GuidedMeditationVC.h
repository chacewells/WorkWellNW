//
//  GuidedMeditationVC.h
//  WorkWell
//
//  Created by Aaron Wells on 7/29/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "PracticeSession.h"
@import AVFoundation;

@class GuidedMeditation;

@interface GuidedMeditationVC : UIViewController <AVAudioPlayerDelegate>

@property (strong, nonatomic) NSManagedObjectID *selectedMeditationID;

@end
