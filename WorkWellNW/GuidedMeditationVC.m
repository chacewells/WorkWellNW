//
//  GuidedMeditationVC.m
//  YogaApp
//
//  Created by Aaron Wells on 7/29/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "GuidedMeditationVC.h"
#import "GuidedMeditation.h"
#import "DateUtils.h"

@interface GuidedMeditationVC ()
@property (strong, nonatomic) AVAudioPlayer *audioPlayer;
@property (strong, nonatomic) NSDate *timeStarted;
@property (weak) NSTimer *progressTimer;
@property (weak, nonatomic) IBOutlet UISlider *progressSlider;
@property (weak, nonatomic) IBOutlet UILabel *progressLabel;

- (IBAction)sliderChanged:(id)sender;
@end

@implementation GuidedMeditationVC
#define debug 1

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.timeStarted = [NSDate date];
    CoreDataHelper *cdh = [(AppDelegate*)[[UIApplication sharedApplication] delegate] cdh];
    GuidedMeditation *guidedMeditation = (GuidedMeditation*)[cdh.context existingObjectWithID:self.selectedMeditationID error:nil];
    self.title = guidedMeditation.title;
    NSData *audioData = guidedMeditation.data;
    self.audioPlayer = [[AVAudioPlayer alloc] initWithData:audioData fileTypeHint:@"mp3" error:nil];
    self.audioPlayer.delegate = self;
    [self.audioPlayer play];
    [self startProgressTimer];
    [self.progressSlider sizeToFit];
}

- (void)startProgressTimer {
    NSTimer *timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateProgressBar:) userInfo:nil repeats:YES];
    self.progressTimer = timer;
}

- (void)endProgressTimer {
    [self.progressTimer invalidate];
}

- (void)updateProgressBar:(NSTimer*)timer {
    self.progressSlider.value = [self progressRatio];
    [self updateProgressLabel];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self endProgressTimer];
    [self savePracticeSession];
}

- (void)savePracticeSession {
    
    CoreDataHelper *cdh = [(AppDelegate*)[[UIApplication sharedApplication] delegate] cdh];
    NSDate *now = [NSDate date];
    NSTimeInterval duration = [now timeIntervalSinceDate:self.timeStarted];
    PracticeSession *practiceSession = [NSEntityDescription insertNewObjectForEntityForName:@"PracticeSession" inManagedObjectContext:cdh.context];
    practiceSession.date = now;
    practiceSession.duration = [NSNumber numberWithDouble:duration];
    practiceSession.guided = [NSNumber numberWithBool:YES];
    
    [cdh saveContext];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (NSTimeInterval) progressRatio {
    return self.audioPlayer.currentTime / self.audioPlayer.duration;
}

- (NSTimeInterval) currentTimeForSliderValue:(double)value {
    return value * self.audioPlayer.duration;
}

- (IBAction)sliderChanged:(id)sender {
    self.audioPlayer.currentTime = [self currentTimeForSliderValue:((UISlider*)sender).value];
}

- (void)audioPlayerDidFinishPlaying:(AVAudioPlayer *)player successfully:(BOOL)flag {
    [[self navigationController] popViewControllerAnimated:YES];
}

- (void)updateProgressLabel {
    self.progressLabel.text =
        [NSString stringWithFormat:@"%@|%@",
         [DateUtils toClockString:self.audioPlayer.currentTime],
         [DateUtils toClockString:self.audioPlayer.duration]];
}

@end
