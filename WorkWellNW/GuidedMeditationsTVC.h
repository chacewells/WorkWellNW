//
//  GuidedMeditationsTVC.h
//  YogaApp
//
//  Created by Aaron Wells on 10/21/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "CoreDataTVC.h"
@class AppDelegate, CoreDataHelper, GuidedMeditation, GuidedMeditationVC;

@interface GuidedMeditationsTVC : CoreDataTVC

@end
