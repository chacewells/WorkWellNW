//
//  CoreDataHelper.h
//  YogaApp
//
//  Created by Aaron Wells on 10/18/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

@import Foundation;
@import CoreData;
@import UIKit;
@class AppDelegate;

@interface CoreDataHelper :NSObject <UIAlertViewDelegate, NSXMLParserDelegate>
@property (nonatomic, readonly) NSManagedObjectContext       *context;
@property (nonatomic, readonly) NSManagedObjectContext       *importContext;
@property (nonatomic, readonly) NSManagedObjectModel         *model;
@property (nonatomic, readonly) NSPersistentStoreCoordinator *coordinator;
@property (nonatomic, readonly) NSPersistentStore            *store;
@property (nonatomic, retain)   UIAlertView                  *importAlertView;
@property (nonatomic, retain)   NSXMLParser                  *parser;

- (void)setupCoreData;
- (void)saveContext;
@end
