//
//  CoreDataUtil.h
//  YogaApp
//
//  Created by Aaron Wells on 10/19/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import <Foundation/Foundation.h>
@class PracticeSession;

@interface CoreDataUtil : NSObject
+ (void)loadMindfulMinuteInstanceTestValues;
+ (void)loadMindfulMinuteTemplateTestValues;
+ (void)loadGuidedMeditations;
+ (void)clearAllMindfulMinuteInstances;
+ (void)clearAllMindfulMinuteTemplates;
+ (void)clearAllGuidedMeditations;
+ (void)clearAllPracticeSessions;
+ (PracticeSession*)practiceSessionWithDate:(NSDate*)date duration:(NSTimeInterval)duration guided:(BOOL)guided;

@end
