//
//  AppDelegate.m
//  YogaApp
//
//  Created by Aaron Wells on 10/18/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "AppDelegate.h"
#import "MindfulMinuteInstance.h"
#import "MindfulMinuteTemplate.h"
#import "GuidedMeditation.h"
#import "CoreDataUtil.h"

@interface AppDelegate ()
@property (nonatomic) BOOL isAppResumingFromBackground;
- (void)showLocalNotification:(UILocalNotification*)notification;
@end

@implementation AppDelegate
#define debug 0

- (void)demo {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    [self cdh];
    [self demo];
    [self nh];
    [self.notificationHelper updateNotifications];
    
//    if (debug == 1) {
        NSArray *notifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
        for ( UILocalNotification *n in notifications ) {
            NSLog(@"notification fire date: %@", [NSDateFormatter localizedStringFromDate:n.fireDate dateStyle:NSDateFormatterShortStyle timeStyle:NSDateFormatterMediumStyle]);
        }
//    }
}

- (NotificationHelper *)nh {
    if (!_notificationHelper) {
        _notificationHelper = [NotificationHelper new];
        _notificationHelper.delegate = self;
    }
    return _notificationHelper;
}

- (CoreDataHelper *)cdh {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    if (!_coreDataHelper) {
        _coreDataHelper = [[CoreDataHelper alloc] init];
        [_coreDataHelper setupCoreData];
    }
    return _coreDataHelper;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    if ([UIApplication instancesRespondToSelector:@selector(registerUserNotificationSettings:)])
    {
        [application registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:UIUserNotificationTypeAlert|UIUserNotificationTypeBadge|UIUserNotificationTypeSound categories:nil]];
    }
    UILocalNotification *notification = (UILocalNotification *)[launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if (notification) {
        [self showLocalNotification:notification];
    }
    self.isAppResumingFromBackground = NO;
    
    return YES;
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification {
    if (notification && self.isAppResumingFromBackground) {
        [self showLocalNotification:notification];
    }
    self.isAppResumingFromBackground = NO;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    [[self cdh] saveContext];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    self.isAppResumingFromBackground = YES;
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

- (void)showLocalNotification:(UILocalNotification*)notification {
    NSLog(@"alerting with notification alertBody");
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Mindful Minute" message:notification.alertBody delegate:nil cancelButtonTitle:@"Done" otherButtonTitles:nil];
    [alert show];
}

@end
