//
//  CoreDataUtil.m
//  YogaApp
//
//  Created by Aaron Wells on 10/19/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "CoreDataUtil.h"
#import "MindfulMinuteInstance.h"
#import "MindfulMinuteTemplate.h"
#import "GuidedMeditation.h"
#import "PracticeSession.h"
#import "AppDelegate.h"

@implementation CoreDataUtil
+ (void)loadMindfulMinuteInstanceTestValues {
    
    CoreDataHelper *cdh = [(AppDelegate*)[[UIApplication sharedApplication] delegate] cdh];
    
    NSTimeInterval hour = 3600.0;
    NSTimeInterval minute = 60.0;
    
    NSTimeInterval times[] = {
        (6 * hour) + (30 * minute),
        (7 * hour) + (30 * minute),
        (8 * hour) + (30 * minute),
        (9 * hour) + (30 * minute),
        (10 * hour) + (30 * minute),
        (11 * hour) + (30 * minute),
        (12 * hour) + (30 * minute)
    };
    
    for (int i = 0 ; i < sizeof(times) / sizeof(times[0]); i++ ) {
        MindfulMinuteInstance *mmi = [NSEntityDescription insertNewObjectForEntityForName:@"MindfulMinuteInstance" inManagedObjectContext:cdh.context];
        mmi.timeOfDay = [NSNumber numberWithDouble:times[i]];
    }
    
    [cdh saveContext];
    
}

+ (void)loadMindfulMinuteTemplateTestValues {
    
    CoreDataHelper *cdh = [(AppDelegate*)[[UIApplication sharedApplication] delegate] cdh];
    
    NSArray *templateGenerics = @[
                                  @{
                                      @"alertBody" : @"Take it easy",
                                      @"soundName" : @"klang.mp3"
                                      },
                                  @{
                                      @"alertBody" : @"Relax",
                                      @"soundName" : @"klang.mp3"
                                      },
                                  @{
                                      @"alertBody" : @"Don't worry",
                                      @"soundName" : @"klang.mp3"
                                      }
                                  ];
    
    for ( NSDictionary *template in templateGenerics ) {
        MindfulMinuteTemplate *mmt = [NSEntityDescription insertNewObjectForEntityForName:@"MindfulMinuteTemplate" inManagedObjectContext:cdh.context];
        mmt.alertBody = template[@"alertBody"];
        mmt.soundName = template[@"soundName"];
    }
    
    [cdh saveContext];
}

+ (void)loadGuidedMeditations {
    CoreDataHelper *cdh = [(AppDelegate*)[[UIApplication sharedApplication] delegate] cdh];
    NSArray *meditations = @[
                             @{
                                 @"title"   :   @"Body Scan",
                                 @"data"    :   @"body_scan"
                                 },
                             @{
                                 @"title"   :   @"Driving",
                                 @"data"    :   @"driving_meditation"
                                 },
                             @{
                                 @"title"   :   @"Intro to Body Scan",
                                 @"data"    :   @"intro_to_body_scan"
                                 },
                             @{
                                 @"title"   :   @"Long Sitting",
                                 @"data"    :   @"long_sitting_meditation"
                                 },
                             @{
                                 @"title"   :   @"Lovingkindness",
                                 @"data"    :   @"lovingkindness_meditation"
                                 },
                             @{
                                 @"title"   :   @"Short Sitting",
                                 @"data"    :   @"short_sitting_meditation"
                                 },
                             @{
                                 @"title"   :   @"Yoga Nidra",
                                 @"data"    :   @"yoga_nidra"
                                 }
                             ];
    
    for ( NSDictionary *dict in meditations ) {
        GuidedMeditation *gm = [NSEntityDescription insertNewObjectForEntityForName:@"GuidedMeditation" inManagedObjectContext:cdh.context];
        gm.title = dict[@"title"];
        NSString *path = [[NSBundle mainBundle] pathForResource:dict[@"data"] ofType:@"mp3"];
        NSLog(@"loadGuidedMeditations: %@", path);
        NSError *error = nil;
        NSData *data = [NSData dataWithContentsOfFile:path options:NSDataReadingMappedIfSafe error:&error];
        if (error)NSLog(@"%@", error);
        gm.data = data;
    }
    
    [cdh saveContext];
}

+ (void)clearAllMindfulMinuteInstances {
    [self clearAllManagedObjectsOfType:@"MindfulMinuteInstance"];
}

+ (void)clearAllMindfulMinuteTemplates {
    [self clearAllManagedObjectsOfType:@"MindfulMinuteTemplate"];
}

+ (void)clearAllGuidedMeditations {
    [self clearAllManagedObjectsOfType:@"GuidedMeditation"];
}

+ (void)clearAllPracticeSessions {
    [self clearAllManagedObjectsOfType:@"PracticeSession"];
}

+ (void)clearAllManagedObjectsOfType:(NSString *)type {
    CoreDataHelper *cdh = [(AppDelegate*)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:type];
    NSError *error = nil;
    NSArray *managedObjects = [cdh.context executeFetchRequest:request error:&error];
    if (error){NSLog(@"Error %@: %@", error, error.localizedDescription);}
    for (NSManagedObject *managedObject in managedObjects)
        [cdh.context deleteObject:managedObject];
    
    [cdh saveContext];
}

#pragma mark - OBJECT BUILDER
+ (PracticeSession*)practiceSessionWithDate:(NSDate*)date duration:(NSTimeInterval)duration guided:(BOOL)guided {
    CoreDataHelper *cdh = [(AppDelegate*)[[UIApplication sharedApplication] delegate] cdh];
    PracticeSession *practiceSession = [NSEntityDescription insertNewObjectForEntityForName:@"PracticeSession" inManagedObjectContext:cdh.context];
    practiceSession.date = date;
    practiceSession.duration = [NSNumber numberWithDouble:duration];
    practiceSession.guided = [NSNumber numberWithBool:guided];
    
    return practiceSession;
}

@end
