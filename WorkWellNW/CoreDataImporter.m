//
//  CoreDataImporter.m
//  YogaApp
//
//  Created by Aaron Wells on 10/27/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "CoreDataImporter.h"

@implementation CoreDataImporter

#define debug 1

+ (void)saveContext:(NSManagedObjectContext*)context {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    [context performBlockAndWait:^{
        if ([context hasChanges]) {
            NSError *error = nil;
            if ([context save:&error]) {
                NSLog(@"CoreDataImporter SAVED changes from context to persistent store");
            }
            else {
                NSLog(@"CoreDataImporter FAILED to save changes from context to persistent store: %@", error);
            }
        }
        else {
            NSLog(@"CoreDataImporter SKIPPED saving context as there are no changes");
        }
    }];
}

- (CoreDataImporter*)initWithUniqueAttributes:(NSDictionary*)uniqueAttributes {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    self.entitiesWithUniqueAttributes = uniqueAttributes;
    return self;
}

- (NSString*)uniqueAttributeForEntity:(NSString*)entity {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    return [self.entitiesWithUniqueAttributes valueForKey:entity];
}

- (NSManagedObject*)existingObjectInContext:(NSManagedObjectContext*)context
                                  forEntity:(NSString*)entity
                   withUniqueAttributeValue:(NSString*)uniqueAttributeValue {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    NSString *uniqueAttribute = [self uniqueAttributeForEntity:entity];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"%K==%@", uniqueAttribute, uniqueAttributeValue];
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:entity];
    [fetchRequest setPredicate:predicate];
    [fetchRequest setFetchLimit:1];
    NSError *error;
    NSArray *fetchRequestResults = [context executeFetchRequest:fetchRequest error:&error];
    if (error) {NSLog(@"Error: %@", error.localizedDescription);}
    if (fetchRequestResults.count == 0) {return nil;}
    return fetchRequestResults.lastObject;
}

- (NSManagedObject*)insertUniqueObjectForEntity:(NSString*)entity
                                      inContext:(NSManagedObjectContext*)context
                            withAttributeValues:(NSDictionary*)attributeValues
                           uniqueAttributeValue:(NSString*)uniqueAttributeValue {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    NSString *uniqueAttribute = [self uniqueAttributeForEntity:entity];
    if (uniqueAttributeValue.length > 0) {
        NSManagedObject *existingObject = [self existingObjectInContext:context
                                                              forEntity:entity
                                               withUniqueAttributeValue:uniqueAttributeValue];
        if (existingObject) {
            NSLog(@"%@ object with %@ value '%@' already exists",entity, uniqueAttribute, uniqueAttributeValue);
            return existingObject;
        } else {
            NSManagedObject *newObject = [NSEntityDescription insertNewObjectForEntityForName:entity inManagedObjectContext:context];
            [newObject setValuesForKeysWithDictionary:attributeValues];
            NSLog(@"Created %@ object with %@ '%@'", entity, uniqueAttribute, uniqueAttributeValue);
            return newObject;
        }
    } else {
        NSLog(@"Skipped %@ object creation because unique attribute value is 0 length", entity);
    }
    return nil;
}

@end
