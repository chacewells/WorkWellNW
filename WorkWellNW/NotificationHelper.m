//
//  NotificationHelper.m
//  YogaApp
//
//  Created by Aaron Wells on 10/19/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NotificationHelper.h"
#import "MindfulMinuteInstance.h"
#import "MindfulMinuteTemplate.h"
#import "CoreDataHelper.h"
#import "DateUtils.h"

@interface NotificationHelper ()
@property (strong, nonatomic)NSDateFormatter *df;

@end

@implementation NotificationHelper

#pragma mark - CONSTANTS
#define debug 0
#define kMaxNotificationsAllowed 64

#pragma mark - SETUP
- (NotificationHelper*)init {
    self.df = [NSDateFormatter new];
    return self;
}

#pragma mark - NOTIFICATIONS
- (void)updateNotifications {
    // TODO: cleanup code and add debugging tools
    if(debug==1) NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    
    // get MindfulMinuteInstances from "database"
    NSArray *mmis = [self allMindfulMinuteInstances];
    if (mmis.count < 1)
        return;
    
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    
    //get notifications from application
    NSArray *notifications = [self allLocalNotifications];
    
    // MARK: skip notification update if remainingAllowableNotificationCount==0
    int totalAllowableNotificationsCount = kMaxNotificationsAllowed / mmis.count;
    int remainingAllowableNotificationsCount = (int)(kMaxNotificationsAllowed - notifications.count) / mmis.count;
    
    if (debug == 1) {NSLog(@"total notifications allowed for each scheduled time: %d\n", remainingAllowableNotificationsCount);}
    
    if (remainingAllowableNotificationsCount < 1)
        return;
    
    // set up a mutable array with each MindfulMinuteInstance as the first element of each
    NSMutableArray *mmArray = [self mutableArrayOfMutableArraysWithMindfulMinuteInstanceAsFirstElement:mmis];
    
    [self addNotifications:notifications toCorrespondingMindfulMinuteInstanceArrays:mmArray];
    
    [self displayMmArrayInfo:mmArray];
    
    // load MindfulMinuteTemplates from "database"
    NSArray *mmts = [self allMindfulMinuteTemplates];
    
    // schedule notifcations in structured array "mmArray"
    [self scheduleNotificationsInSubarrays:mmArray withMindfulMinuteTemplates:mmts forTotalAllowableNotifications:totalAllowableNotificationsCount];
}

- (NSArray*)allMindfulMinuteInstances {
    if(debug==1) NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    // get MindfulMinuteInstances from "database"
    CoreDataHelper *cdh = [self.delegate cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"MindfulMinuteInstance"];
    NSError *error = nil;
    
    NSComparator mmiComparator = ^(id obj1, id obj2) {
        if ([[obj1 timeOfDay] compare:[obj2 timeOfDay]] > NSOrderedSame) {
            return NSOrderedDescending;
        }
        if ([[obj1 timeOfDay] compare:[obj2 timeOfDay]] < NSOrderedSame) {
            return NSOrderedAscending;
        }
        return NSOrderedSame;
    };
    NSArray *mmis = [[cdh.context executeFetchRequest:request error:&error] sortedArrayUsingComparator:mmiComparator];
    return mmis;
}

- (NSArray*)allMindfulMinuteTemplates {
    if(debug==1) NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    // load MindfulMinuteTemplates from "database"
    CoreDataHelper *cdh = [self.delegate cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"MindfulMinuteTemplate"];
    NSError *error = nil;
    NSArray *mmts = [cdh.context executeFetchRequest:request error:&error];
    
    if (error && debug==1) NSLog(@"Error: %@ '%@'", error, [error localizedDescription]);
    
    return mmts;
}

- (NSArray*)allLocalNotifications {
    if(debug==1) NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    NSComparator notificationComparator = ^(id obj1, id obj2) {
        if ([[obj1 fireDate] compare:[obj2 fireDate]] > NSOrderedSame)
            return NSOrderedDescending;
        if ([[obj1 fireDate] compare:[obj2 fireDate]] < NSOrderedSame)
            return NSOrderedAscending;
        
        return NSOrderedSame;
    };
    
    //get notifications from application
    NSArray *notifications = [[[UIApplication sharedApplication] scheduledLocalNotifications] sortedArrayUsingComparator:notificationComparator];
    return notifications;
}

- (NSMutableArray*)mutableArrayOfMutableArraysWithMindfulMinuteInstanceAsFirstElement:(NSArray*)mmis {
    if(debug==1) NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    NSMutableArray *mmArray = [NSMutableArray new];
    NSMutableArray *mmSubArray;
    for (int i = 0; i < mmis.count; i++) {
        mmSubArray = [NSMutableArray new];
        [mmSubArray addObject:[mmis objectAtIndex:i]];
        [mmSubArray addObject:[NSMutableArray new]];
        [mmArray addObject:mmSubArray];
        if (debug==1) {
            NSLog(@"MindfulMinute Instance %d: %@", i, [_df stringFromDate:[[DateUtils startOfDayWithDate:[NSDate date]] dateByAddingTimeInterval:[[[mmis objectAtIndex:i] timeOfDay] doubleValue]]]);
        }
    }
    
    return [mmArray copy];
}

- (void)addNotifications:(NSArray*)notifications toCorrespondingMindfulMinuteInstanceArrays:(NSMutableArray*)mmArray {
    if(debug==1) NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    if (debug==1) {NSLog(@"notifications count: %ld", (unsigned long)notifications.count);}
    
    // assign notifications to arrays in mmArray
    // MARK: cancel notifications without corresponding MindfulMinuteInstances
    for (UILocalNotification *notification in notifications) {
        BOOL notificationHasCorrespondingMindfulMinuteInstance = NO;
        for (NSMutableArray *mmSubArray in mmArray) {
            NSMutableArray *notificationArray = [mmSubArray objectAtIndex:1];
            NSTimeInterval mmiTimeOfDay = [[[mmSubArray objectAtIndex:0] timeOfDay] doubleValue];
            NSTimeInterval notificationFireDateTI = [DateUtils timeIntervalFromHoursAndMinutesOfDate:notification.fireDate];
            if (notificationFireDateTI == mmiTimeOfDay) {
                if (debug==1) {NSLog(@"mmiTimeOfDay: %f; notificationFireDateTI: %f", mmiTimeOfDay, notificationFireDateTI);}
                [notificationArray addObject:notification];
                notificationHasCorrespondingMindfulMinuteInstance = YES;
                break;
            }
            // MARK: cancel notification without instance backing it up
        }
        
        if (!notificationHasCorrespondingMindfulMinuteInstance) {
            [[UIApplication sharedApplication] cancelLocalNotification:notification];
        }
    }
    
    if (debug==1) {
        for (NSMutableArray *mmSubArray in mmArray) {
            NSLog(@"objects in subarray: %ld", (unsigned long)[[mmSubArray objectAtIndex:1] count]);
        }
    }
}

- (void)scheduleNotificationsInSubarrays:(NSMutableArray*)mmArray withMindfulMinuteTemplates:(NSArray*)mmts forTotalAllowableNotifications:(int)totalAllowableNotificationsCount {
    if(debug==1) NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));
    // build and schedule notifications
    // MARK: exclude Saturday & Sunday notification scheduling
    for (NSMutableArray *mmSubArray in mmArray) {
        for (int i = (int)([[mmSubArray objectAtIndex:1] count]); i < totalAllowableNotificationsCount; i++) {
            NSDate *date;
            if (i == 0) {
//                weekday only code
//                date = [DateUtils nextWeekdayWithTimeOfDayFromTimeInterval:[[[mmSubArray objectAtIndex:0] timeOfDay] doubleValue]];
//                includes weekends
                date = [DateUtils nextDateWithTimeOfDayFromTimeInterval:[[[mmSubArray objectAtIndex:0] timeOfDay] doubleValue]];
            } else {
                //                weekday only code
                date = [DateUtils dateByAddingDays:1 toDate:[DateUtils nextWeekdayAfterDate:[mmSubArray[1][(i-1)] fireDate]]];
//                includes weekends
                date = [DateUtils dateByAddingDays:1 toDate:[mmSubArray[1][(i-1)] fireDate]];
            }
            //            NSLog(@"mmSubArray.count index 1: %d - supposed notificationArray index: %d", [mmSubArray[1] count], i-1);
            
            UILocalNotification *notification = [UILocalNotification new];
            notification.fireDate = date;
            if (mmts.count > 0) {
                MindfulMinuteTemplate *mmt = [DateUtils randomObjectFromArray:mmts];
                notification.alertBody = mmt.alertBody;
                notification.soundName = mmt.soundName;
            } else {
                notification.alertBody = @"Breathe deep and let out all your worries.";
            }
            
            [[UIApplication sharedApplication] scheduleLocalNotification:notification];
            [[mmSubArray objectAtIndex:1] addObject:notification];
            if (debug==1) {
                //                NSLog(@"%d '%@': %@", i, theNotification.alertBody, theNotification.soundName);
                NSLog(@"notification scheduled: %@ '%@'", [_df stringFromDate:notification.fireDate], notification.alertBody);
            }
        }
    }
    
}

#pragma mark - DEBUG
- (void)displayObjectCountInSubArrayForArray:(NSMutableArray*)mmArray {
    if (debug==1) {
        for (NSMutableArray *mmSubArray in mmArray) {
            NSLog(@"objects in subarray: %ld", (unsigned long)[[mmSubArray objectAtIndex:1] count]);
        }
    }
}

- (void)displayNotificationInfoForObjectsInArray:(NSMutableArray*)mmArray {
    if (debug==1) {
        int i = 1;
        for (NSMutableArray *mmSubArray in mmArray) {
            int j = 1;
            for (UILocalNotification *theNotification in [mmSubArray objectAtIndex:1]) {
                NSLog(@"set %d - notification %d: %@", i, j, [_df stringFromDate:theNotification.fireDate]);
                j++;
            }
            i++;
        }
    }
}

- (void)displayObjectClassForObjectsInSubarrayForArray:(NSMutableArray*)mmArray {
    if (debug==1) {
        NSLog(@"mmArray count: %ld", (unsigned long)mmArray.count);
        int i = 0;
        for (NSMutableArray *mmSubArray in mmArray) {
            for (id object in mmSubArray) {
                NSLog(@"%@", [object class]);
            }
            i++;
        }
    }
}

- (void)displayMmArrayInfo:(NSMutableArray*)mmArray {
    [self displayNotificationInfoForObjectsInArray:mmArray];
    [self displayObjectCountInSubArrayForArray:mmArray];
    [self displayObjectClassForObjectsInSubarrayForArray:mmArray];
}

- (void)showScheduledNotifications {
    NSArray *notifications = [[UIApplication sharedApplication] scheduledLocalNotifications];
    for ( UILocalNotification *n in notifications )
        NSLog(@"%@", n);
}

@end
