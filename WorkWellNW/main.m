//
//  main.m
//  WorkWellNW
//
//  Created by Aaron Wells on 1/28/15.
//  Copyright (c) 2015 Aaron Wells. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
