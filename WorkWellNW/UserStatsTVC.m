//
//  UserStatsTVC.m
//  YogaApp
//
//  Created by Aaron Wells on 10/23/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "UserStatsTVC.h"
#import "AppDelegate.h"
#import "PracticeSession.h"
#import "PracticeSessions.h"
#import "DateUtils.h"

@interface UserStatsTVC ()

@end

@implementation UserStatsTVC
#define debug 1

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [PracticeSessions syncPracticeSessions];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
        return 3;
}

- (NSString*)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
        return @"User Stats";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    
    cell = [tableView dequeueReusableCellWithIdentifier:@"Stats Cell" forIndexPath:indexPath];
    // MARK: TOTAL SESSIONS CELL
    if (indexPath.row == 0) {
        cell.textLabel.text = @"Total Sessions to Date";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%lu", (long)[PracticeSessions count]];
        // MARK: TIME PRACTICED CELL
    } else if (indexPath.row == 1) {
        NSDictionary *userHMS = [PracticeSessions totalTimePracticed];
        cell.textLabel.text = @"Total Time Practiced";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)[[userHMS objectForKey:@"hours"] integerValue], (long)[[userHMS objectForKey:@"minutes"] integerValue], (long)[[userHMS objectForKey:@"seconds"] integerValue]];
        // MARK: SESSIONS PER WEEK CELL
    } else {
        NSInteger weeks = [PracticeSessions weeksSinceFirstPracticeSession];
        weeks += 1;
        cell.textLabel.text = @"Sessions Per Week";
        cell.detailTextLabel.text = [NSString stringWithFormat:@"%ld", ([PracticeSessions count] / weeks) ];
    }
    // MARK: CURRENT USER CELL

    return cell;
}

/*
 // Override to support conditional editing of the table view.
 - (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the specified item to be editable.
 return YES;
 }
 */

/*
 // Override to support editing the table view.
 - (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
 {
 if (editingStyle == UITableViewCellEditingStyleDelete) {
 // Delete the row from the data source
 [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
 } else if (editingStyle == UITableViewCellEditingStyleInsert) {
 // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
 }
 }
 */

/*
 // Override to support rearranging the table view.
 - (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
 {
 }
 */

/*
 // Override to support conditional rearranging of the table view.
 - (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
 {
 // Return NO if you do not want the item to be re-orderable.
 return YES;
 }
 */

@end
