//
//  CoreDataImporter.h
//  YogaApp
//
//  Created by Aaron Wells on 10/27/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//
@import Foundation;
#import "AppDelegate.h"

@interface CoreDataImporter : NSObject
@property (nonatomic, retain) NSDictionary *entitiesWithUniqueAttributes;

+ (void)saveContext:(NSManagedObjectContext*)context;
- (CoreDataImporter*)initWithUniqueAttributes:(NSDictionary*)uniqueAttributes;
- (NSString*)uniqueAttributeForEntity:(NSString*)entity;
- (NSManagedObject*)insertUniqueObjectForEntity:(NSString*)entity
                                      inContext:(NSManagedObjectContext*)context
                            withAttributeValues:(NSDictionary*)attributeValues
                           uniqueAttributeValue:(NSString*)uniqueAttributeValue;
@end
