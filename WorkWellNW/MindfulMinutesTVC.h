//
//  MindfulMinuteTVC.h
//  YogaApp
//
//  Created by Aaron Wells on 10/19/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "CoreDataTVC.h"

@interface MindfulMinutesTVC : CoreDataTVC <UIActionSheetDelegate>
@property(strong, nonatomic) UIActionSheet *clearConfirmActionSheet;

@end
