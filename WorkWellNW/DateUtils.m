//
//  DateUtils.m
//  YogaApp
//
//  Created by Aaron Wells on 10/19/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "DateUtils.h"

@implementation DateUtils

#define YEAR_MONTH_DAY_HOUR_MINUTE_SECOND_COMPONENTS NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond
#define HOURS_MINUTES_SECONDS NSCalendarUnitHour|NSCalendarUnitMinute|NSCalendarUnitSecond
#define YEAR_MONTH_DAY_COMPONENTS NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay

#pragma mark - DATE CONVERSIONS
+ (NSDate*)dateByAddingDays:(NSUInteger)days toDate:(NSDate*)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:YEAR_MONTH_DAY_HOUR_MINUTE_SECOND_COMPONENTS fromDate:date];
    components.day += days;
    
    return [calendar dateFromComponents:components];
}

+ (NSDictionary *)hoursMinutesSeconds:(NSTimeInterval)timeInterval {
    NSInteger hours, minutes, seconds;
    seconds = (NSInteger) timeInterval;
    hours = seconds / 3600;
    seconds %= 3600;
    minutes = seconds / 60;
    seconds %= 60;
    return @{@"hours":[NSNumber numberWithInteger:hours],@"minutes":[NSNumber numberWithInteger:minutes],@"seconds":[NSNumber numberWithInteger:seconds]};
}

+ (NSString *)toClockString:(NSTimeInterval)timeInterval {
    NSDictionary *hms = [self hoursMinutesSeconds:timeInterval];
    NSString *clockString = [NSString stringWithFormat:@"%02ld:%02ld", [[hms objectForKey:@"minutes"] integerValue], [[hms objectForKey:@"seconds"] integerValue]];
    NSInteger hours = [[hms objectForKey:@"hours"] integerValue];
    clockString = (hours > 0) ? [NSString stringWithFormat:@"%02ld:%@", hours, clockString] : clockString;
    return clockString;
}

+ (NSDate*)nextDateWithTimeOfDayFromDate:(NSDate*)theDate {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDate *now = [NSDate date];
    NSDateComponents *theDateComponents = [calendar components:HOURS_MINUTES_SECONDS fromDate:theDate],
    *nowComponents = [calendar components:YEAR_MONTH_DAY_HOUR_MINUTE_SECOND_COMPONENTS fromDate:now];
    [theDateComponents setYear:[nowComponents year]];
    [theDateComponents setMonth:[nowComponents month]];
    [theDateComponents setDay:[nowComponents day]];
    
    theDate = [calendar dateFromComponents:theDateComponents];
    
    if ([theDate compare:now] < 0) {
        [theDateComponents setDay:(theDateComponents.day + 1)];
        theDate = [calendar dateFromComponents:theDateComponents];
    }
    
    return theDate;
}

+ (NSDate*)nextDateWithTimeOfDayFromTimeInterval:(NSTimeInterval)timeInterval {
    NSDate *today = [self startOfDayWithDate:[NSDate date]];
    return [today dateByAddingTimeInterval:timeInterval];
}

+ (NSDate*)nextWeekdayAfterDate:(NSDate*)date {
    NSDate *next;
    NSInteger weekday = [self weekdayFromDate:date];
    
    switch (weekday) {
        case 1:
            next = [self dateByAddingDays:1 toDate:date];
            break;
        case 7:
            next = [self dateByAddingDays:2 toDate:date];
            break;
        default:
            next = date;
            break;
    }
    
    return next;
}

+ (NSDate*)nextWeekdayWithTimeOfDayFromTimeInterval:(NSTimeInterval)timeInterval {
    NSDate *date = [self nextDateWithTimeOfDayFromTimeInterval:timeInterval];
    return [self nextWeekdayAfterDate:date];
}

+ (NSDate*)startOfDayWithDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay) fromDate:date];
    return [calendar dateFromComponents:components];
}

+ (NSTimeInterval)timeIntervalFromHoursAndMinutesOfDate:(NSDate *)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitHour|NSCalendarUnitMinute fromDate:date];
    
    NSTimeInterval interval = [self timeIntervalWithHours:components.hour minutes:components.minute seconds:0];
    return interval;
}

+ (NSTimeInterval)timeIntervalWithHours:(NSUInteger)hours minutes:(NSUInteger)minutes seconds:(NSUInteger)seconds {
    NSUInteger secs = (NSTimeInterval)seconds;
    secs = secs + (minutes * 60);
    secs = secs + (hours * 3600);
    return (NSTimeInterval)secs;
}

+ (NSDictionary *)timeOfDayWithPeriod:(NSTimeInterval)timeInterval {
    NSMutableDictionary *result = [NSMutableDictionary dictionaryWithDictionary:[self hoursMinutesSeconds:timeInterval]];
    NSString * period;
    if ([[result objectForKey:@"hours"] integerValue] > 11) {
        period = @"PM";
    } else {
        period = @"AM";
    }
    
    switch ([[result objectForKey:@"hours"] integerValue]) {
        case 0:
        case 12:
            [result setObject:[NSNumber numberWithInt:12] forKey:@"hours"];
            break;
        default:
            break;
    }
    
    if ([[result objectForKey:@"hours"] integerValue] > 23) {
        return nil;
    }
    
    [result setObject:period forKey:@"period"];
    return (NSDictionary*)result;
}

+ (NSInteger)weekdayFromDate:(NSDate*)date {
    NSDateComponents *weekdayComponent = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekday fromDate:date];
    return [weekdayComponent weekday];
}

#pragma mark - UNIX TIME STRINGS
+ (NSString*)UNIXFormattedStringFromDate:(NSDate*)date {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    return [dateFormatter stringFromDate:date];
}

+ (NSString*)UNIXformattedStringWithUTCTimeFromDate:(NSDate*)date {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    [calendar setTimeZone:[NSTimeZone timeZoneWithName:@"UTC"]];
    NSDateComponents *components = [calendar components:YEAR_MONTH_DAY_HOUR_MINUTE_SECOND_COMPONENTS fromDate:date];
    
    [calendar setTimeZone:[NSTimeZone systemTimeZone]];
    date = [calendar dateFromComponents:components];
    
    return [self UNIXFormattedStringFromDate:date];
}

#pragma mark - ARRAYS
+ (id)randomObjectFromArray:(NSArray*)array {
    return [array objectAtIndex:(arc4random() % array.count)];
}
@end


