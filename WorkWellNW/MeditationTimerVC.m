//
//  MeditationTimerVC.m
//  YogaApp
//
//  Created by Aaron Wells on 10/22/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "MeditationTimerVC.h"
#import "CoreDataUtil.h"
@import AVFoundation;

@interface MeditationTimerVC ()
@property (weak, nonatomic) IBOutlet UILabel *labelCountdown;
@property (weak, nonatomic) IBOutlet UITextField *fieldMinutes;
@property (weak, nonatomic) IBOutlet UITextField *fieldPacer;
@property (weak, nonatomic) IBOutlet UIButton *buttonStart;
@property (weak) NSTimer *countdownTimer;
@property (weak) NSTimer *pacerTimer;
@property (strong, nonatomic) AVAudioPlayer *pacerPlayer;
@property SystemSoundID pacerSound;
@property NSTimeInterval timeLeft;
@property NSTimeInterval timeElapsed;
@property NSInteger alertsPlayed;
@property BOOL timerSessionIsInProgress;

- (IBAction)handleTimerStartButtonClick:(id)sender;
- (IBAction)handleTimerStopButtonClick:(id)sender;
- (IBAction)keyboardHide:(id)sender;
- (void)updateTime:(NSTimer*)theTimer;
@end

@implementation MeditationTimerVC
#define debug 1
#define kSecondsPerMinute 60.0
#define kAlertSound @"klang"

- (void)viewDidLoad
{
    [super viewDidLoad];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TIMERS
- (IBAction)handleTimerStartButtonClick:(id)sender {
    if ( [self isTimerSessionInProgress] ) {
        if ( [self countdownTimerIsRunning] ) {
            [self pauseTimer];
        } else {
            [self startCountdownTimer];
        }
    } else {
        [self startSession];
    }
}

- (IBAction)handleTimerStopButtonClick:(id)sender {
        [self stopSession];
}

- (void)startPacerTimer {
    if (self.pacerTimer != nil)
        [self.pacerTimer invalidate];
    
    NSTimeInterval pacerInterval = [self.fieldPacer.text doubleValue] * 6.0;
    
    if (pacerInterval < 1) {
        [self stopSession];
        UIAlertView *pacerAlert = [[UIAlertView alloc] initWithTitle:@"Invalid Pacer Time" message:@"Cannot initialize the pacer with the value set. Please set the pace for at least a minute before starting the timer." delegate:nil cancelButtonTitle:@"Okay" otherButtonTitles:nil];
        [pacerAlert show];
    } else {
        NSTimer *timer=[NSTimer scheduledTimerWithTimeInterval:pacerInterval target:self selector:@selector(playAlertOrStopTimer:) userInfo:nil repeats:YES];
        
        self.pacerTimer = timer;
    }
}

- (void)startCountdownTimer {
    if ( self.countdownTimer != nil || [self.countdownTimer isValid]) {
        [self.countdownTimer invalidate];
    }
    
    NSTimer *timer;
    timer=[NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTime:) userInfo:nil repeats:YES];
    self.countdownTimer = timer;
    [self toggleSessionInProgress];
}

- (void)startSession {
    [self resetCounters];
    [self startCountdownTimer];
    [self startPacerTimer];
}

- (void)stopSession {
    if ([self isTimerSessionInProgress]) {
        [self.countdownTimer invalidate];
        [self.pacerTimer invalidate];
        [self recordSessionInContext];
        [self toggleSessionInProgress];
        [self clearDisplay];
        [self releaseAudioPlayer];
    }
}

- (void)resetCounters {
    self.timeLeft = [self.fieldMinutes.text doubleValue] * kSecondsPerMinute;
    self.timeElapsed = 0.0;
    [self updateCountdownLabel];
}

- (void)clearDisplay {
    self.timeLeft = self.timeElapsed = 0;
    [self updateCountdownLabel];
}

- (void)pauseTimer {
    [self.countdownTimer invalidate];
    [self.pacerTimer invalidate];
    [self recordSessionInContext];
    self.timeElapsed = 0.0;
}

#pragma mark - VIEW
- (void)updateTime:(NSTimer*)theTimer {
    
    if (self.timeLeft >= 1.0) {
        --self.timeLeft;
        ++self.timeElapsed;
    } else {
        [theTimer invalidate];
        self.labelCountdown.text = @"00:00";
    }
    
    if ((NSInteger)self.timeLeft % 60 == 0) {
        if (![self.pacerTimer isValid]) {
            [self playAlertSound];
            [self startPacerTimer];
        }
    }
    
    [self updateCountdownLabel];
}

- (void)updateCountdownLabel {
    NSInteger hoursLeft, minutesLeft, secondsLeft;
    NSDictionary *hms = [DateUtils hoursMinutesSeconds:self.timeLeft];
    hoursLeft = [[hms objectForKey:@"hours"] integerValue];
    minutesLeft = [[hms objectForKey:@"minutes"] integerValue];
    secondsLeft = [[hms objectForKey:@"seconds"] integerValue];
    
    if (hoursLeft > 0)
        self.labelCountdown.text = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", (long)hoursLeft, (long)minutesLeft, (long)secondsLeft];
    else
        self.labelCountdown.text = [NSString stringWithFormat:@"%02ld:%02ld", (long)minutesLeft, (long)secondsLeft];
}

- (IBAction)keyboardHide:(id)sender {
    [self.fieldMinutes resignFirstResponder];
    [self.fieldPacer resignFirstResponder];
}

- (void)playAlertOrStopTimer:(NSTimer*)theTimer {
    [self playAlertSound];
    
    if (self.timeLeft < 1.0) {
        [self recordSessionInContext];
        [theTimer invalidate];
        [self playAlertSound];
        self.alertsPlayed = 1;
        self.pacerTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(playAlert:) userInfo:nil repeats:YES];
    }
}

- (void)releaseAudioPlayer {
    [self.pacerPlayer stop];
    self.pacerPlayer = nil;
}

#pragma mark - EVENTS
- (void)playAlert:(NSTimer*)theTimer {
    if (self.alertsPlayed > 2) {
        [theTimer invalidate];
        self.alertsPlayed = 0;
        return;
    }
    
    [self playAlertSound];
    
    self.alertsPlayed++;
}

- (void)playAlertSound {
    [self loadSound];
    if (self.pacerPlayer != nil) {
        [self.pacerPlayer play];
    } else {
        NSLog(@"cannot play sound with player: '%@'", kAlertSound);
    }
}

- (void)loadSound {
    NSString *soundFile;
    self.pacerPlayer =
        (self.pacerPlayer == nil &&
         [[NSFileManager defaultManager] fileExistsAtPath:(soundFile=[[NSBundle mainBundle] pathForResource:kAlertSound ofType:@"mp3"])]) ?
        [[AVAudioPlayer alloc] initWithContentsOfURL:[NSURL fileURLWithPath:soundFile] error:nil] :
        self.pacerPlayer;
}

#pragma mark - USER DATA
- (void)recordSessionInContext {
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    CoreDataHelper *cdh = [appDelegate cdh];
    PracticeSession *ps = [NSEntityDescription insertNewObjectForEntityForName:@"PracticeSession" inManagedObjectContext:cdh.context];
    ps.date = [NSDate date];
    ps.duration = [NSNumber numberWithDouble:self.timeElapsed];
    ps.guided = [NSNumber numberWithBool:NO];
    [cdh saveContext];
}

#pragma mark - boolean helpers
- (void)toggleSessionInProgress {
    self.timerSessionIsInProgress = !self.timerSessionIsInProgress;
}

- (BOOL)isTimerSessionInProgress {
    return self.timerSessionIsInProgress;
}

- (BOOL)countdownTimerIsRunning {
    BOOL result = false;
    if (self.countdownTimer != nil)
        result = [self.countdownTimer isValid];
    
    return result;
}

@end
