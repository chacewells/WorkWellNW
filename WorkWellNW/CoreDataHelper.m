//
//  CoreDataHelper.m
//  WorkWellNW
//
//  Created by Aaron Wells on 10/18/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "CoreDataHelper.h"
#import "AppDelegate.h"
#import "CoreDataImporter.h"

@implementation CoreDataHelper

#define debug 1

#pragma mark - FILES
NSString * kStoreFilename = @"WorkWellNW.sqlite";

#pragma mark - DIRECTORIES
- (NSString *)applicationDocumentsDirectory {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    return [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask,YES) lastObject];
}

- (NSURL *)applicationStoresDirectory {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    NSURL *storesDirectory = [[NSURL fileURLWithPath:[self applicationDocumentsDirectory]] URLByAppendingPathComponent:@"Stores"];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:[storesDirectory path]]) {
        NSError *error = nil;
        if ([fileManager createDirectoryAtURL:storesDirectory withIntermediateDirectories:YES attributes:nil error:&error]) {
            if (debug==1) {NSLog(@"Successfully created Stores directory");}
        }
        else {NSLog(@"FAILED to create Stores directory: %@", error);}
    }
    else {if (debug==1) {NSLog(@"Skipped creating Stores directory (it already exists)");}}
    return storesDirectory;
}

- (NSURL *)storeURL {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    return [[self applicationStoresDirectory] URLByAppendingPathComponent:kStoreFilename];
}

#pragma mark - SETUP
- (id)init {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    self = [super init];
    if (!self) {return nil;}
    _model = [NSManagedObjectModel mergedModelFromBundles:nil];
    _coordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:_model];
    _context = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
    [_context setPersistentStoreCoordinator:_coordinator];
    
    _importContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [_importContext performBlockAndWait:^{
        [_importContext setPersistentStoreCoordinator:_coordinator];
        [_importContext setUndoManager:nil]; // the default on iOS
    }];
    
    return self;
}

- (void)loadStore {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    NSError *error = nil;
    _store = [_coordinator addPersistentStoreWithType:NSSQLiteStoreType
                                        configuration:nil
                                                  URL:[self storeURL]
                                              options:nil error:&error];
    if (!_store) {NSLog(@"Failed to add store. Error: %@", error);abort();}
    else         {if (debug==1) {NSLog(@"Successfully added store: %@", _store);}}
}

- (void)setupCoreData {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    [self setDefaultDataStoreAsInitialStore];
    [self loadStore];
//    [self checkIfDefaultDataNeedsImporting];
}

#pragma mark - SAVING
- (void)saveContext {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    if ([_context hasChanges]) {
        NSError *error = nil;
        if ([_context save:&error]) {NSLog(@"_context SAVED changes to persistent store");}
        else {NSLog(@"Failed to save _context: %@", error);abort();}
    }
}

#pragma mark – DATA IMPORT
- (BOOL)isDefaultDataAlreadyImportedForStoreWithURL:(NSURL*)url ofType:(NSString*)type {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    NSError *error;
    NSDictionary *dictionary =
    [NSPersistentStoreCoordinator metadataForPersistentStoreOfType:type
                                                               URL:url
                                                             error:&error];
    if (error) {
        NSLog(@"Error reading persistent store metadata: %@", error.localizedDescription);
    }
    else {
        NSNumber *defaultDataAlreadyImported = [dictionary valueForKey:@"DefaultDataImported"];
        if (!defaultDataAlreadyImported.boolValue) {
            NSLog(@"Default Data has NOT already been imported");
            return NO;
        }
    }
    if (debug==1) {NSLog(@"Default Data HAS already been imported");}
    return YES;
}

- (void)checkIfDefaultDataNeedsImporting {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    if (![self isDefaultDataAlreadyImportedForStoreWithURL:[self storeURL] ofType:NSSQLiteStoreType]) {
        self.importAlertView = [[UIAlertView alloc] initWithTitle:@"Load Default Data?"
                                                          message:@"If you've never used WorkWell NW before then some default data might help you understand how to use it. Tap 'Yes' to import default data. Tap 'No' to skip the import, especially if you've done this before on other devices."
                                                         delegate:self
                                                cancelButtonTitle:@"No"
                                                otherButtonTitles:@"Yes", nil];
        [self.importAlertView show];
    }
}

- (void)importFromXML:(NSURL*)url {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    self.parser = [[NSXMLParser alloc] initWithContentsOfURL:url];
    self.parser.delegate = self;
    NSLog(@"**** START PARSE OF %@", url.path);
    [self.parser parse];
    NSLog(@"***** END PARSE OF %@", url.path);
}

- (void)setDefaultDataAsImportedForStoreWithURL:(NSURL*)url ofType:(NSString*)type {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    NSError *error;
    // get copy of store metadata dictionary
    NSMutableDictionary *dictionary =
    [[NSPersistentStoreCoordinator metadataForPersistentStoreOfType:type
                                                                URL:url
                                                              error:&error] mutableCopy];
    if (error) {
        NSLog(@"Error reading Persistent Store Metadata: %@", error.localizedDescription);
    }
    else {
        // update dictionary with new key
        error = nil;
        NSLog(@"__Store Metadata BEFORE changes__ \n %@", dictionary);
        [dictionary setObject:[NSNumber numberWithBool:YES] forKey:@"DefaultDataImported"];
        // set store metadata dictionary
        [NSPersistentStoreCoordinator setMetadata:dictionary
                         forPersistentStoreOfType:type
                                              URL:url
                                            error:&error];
        if (error) {
            NSLog(@"Error writing Persistent Store Metadata: %@", error.localizedDescription);
        }
        else {
            NSLog(@"Successfully set DefaultDataImported key to YES for store: %@", url.path);
            NSLog(@"__Store Metadata AFTER changes__ \n %@", dictionary);
        }
    }
}

- (void)setDefaultDataAsNotImportedForStoreWithURL:(NSURL*)url ofType:(NSString*)type {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    NSError *error;
    // get copy of store metadata dictionary
    NSMutableDictionary *dictionary =
    [[NSPersistentStoreCoordinator metadataForPersistentStoreOfType:type
                                                                URL:url
                                                              error:&error] mutableCopy];
    if (error) {
        NSLog(@"Error reading Persistent Store Metadata: %@", error.localizedDescription);
    }
    else {
        // update dictionary with new key
        error = nil;
        NSLog(@"__Store Metadata BEFORE changes__ \n %@", dictionary);
        [dictionary setObject:[NSNumber numberWithBool:NO] forKey:@"DefaultDataImported"];
        // set store metadata dictionary
        [NSPersistentStoreCoordinator setMetadata:dictionary
                         forPersistentStoreOfType:type
                                              URL:url
                                            error:&error];
        if (error) {
            NSLog(@"Error writing Persistent Store Metadata: %@", error.localizedDescription);
        }
        else {
            NSLog(@"Successfully set DefaultDataImported key to YES for store: %@", url.path);
            NSLog(@"__Store Metadata AFTER changes__ \n %@", dictionary);
        }
    }
}

- (void)setDefaultDataStoreAsInitialStore {
    if (debug==1) {NSLog(@"Running %@ '%@'", self.class, NSStringFromSelector(_cmd));}
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:self.storeURL.path]) {
        NSURL *defaultDataPrimaryURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"WorkWellNW" ofType:@"sqlite"]];
        NSError *error;
        if (![fileManager copyItemAtURL:defaultDataPrimaryURL toURL:self.storeURL error:&error]) {
            NSLog(@"%@ copy FAIL: %@", kStoreFilename, error.localizedDescription);
        }
        else {
            NSLog(@"A copy of DefaultData.sqlite was set as the initial store for %@", self.storeURL.path);
        }
    }
    else{
        NSLog(@"%@ already exists on the device. DefaultData.sqlite copy was skipped.",self.storeURL.path);
    }
}

#pragma mark - DELEGATE: UIAlertView

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    if (alertView == self.importAlertView) {
        if (buttonIndex == 1) { // YES on the importAlertView
            NSLog(@"Default Data Import Approved by User");
            [_importContext performBlock:^{
                // XML Import
                [self importFromXML:[[NSBundle mainBundle] URLForResource:@"DefaultData" withExtension:@"xml"]];
            }];
        }
        else
        {
            NSLog(@"Default Data Import Cancelled by User");
        }
        // Set the data as imported regardless of the user's decision, so they're not harassed by this alert.
        [self setDefaultDataAsImportedForStoreWithURL:[self storeURL] ofType:NSSQLiteStoreType];
    }
}

#pragma mark - DELEGATE: NSXMLParser (This code Grocery Dude data specific)
- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError {
    if (debug==1) {NSLog(@"Parser Error: %@", parseError.localizedDescription);}
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName attributes:(NSDictionary *)attributeDict {
    
    [self.importContext performBlockAndWait:^{
        
        CoreDataHelper *cdh;
        
        // STEP 1: Ensure this delegate call is for the item element
        if ([elementName isEqualToString:@"mindfulminutetemplate"]) {
            cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
            
            // STEP 2: Choose the unique attribute names
            NSString *mindfulMinuteTemplateUniqueAttributeValue = [attributeDict valueForKey:@"alertBody"];
            
            // STEP 3: Configure unique attribute for each entity
            NSArray *entities = [NSArray arrayWithObjects:@"MindfulMinuteTemplate", nil];
            NSArray *uniqueAttributes = [NSArray arrayWithObjects: @"alertBody", nil];
            NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:uniqueAttributes forKeys:entities];
            
            // STEP 4: Configure attribute values to give the new objects.
            NSArray *mindfulMinuteTemplateAttributes = [NSArray arrayWithObjects: @"alertBody", @"soundName", nil];
            NSArray *mindfulMinuteTemplateValues = [NSArray arrayWithObjects: [attributeDict objectForKey:@"alertBody"], [attributeDict objectForKey:@"soundName"], nil];
            NSDictionary *mindfulMinuteTemplateAttributeValues = [NSDictionary dictionaryWithObjects:mindfulMinuteTemplateValues forKeys:mindfulMinuteTemplateAttributes];
            
            // STEP 5: Create an instance of CoreData Importer
            CoreDataImporter *importer = [[CoreDataImporter alloc] initWithUniqueAttributes:dictionary];
            
            // STEP 6: Create new unique objects
            NSManagedObject *mindfulMinuteTemplate = [importer insertUniqueObjectForEntity:@"MindfulMinuteTemplate" inContext:cdh.importContext withAttributeValues:mindfulMinuteTemplateAttributeValues uniqueAttributeValue:mindfulMinuteTemplateUniqueAttributeValue];
            
            // STEP 7: Relate related objects
            
            // STEP 8: Save new objects to the persistent store. This sends the NSManagedObjectContextDidSaveNotification, which triggers a fetch on Core Data table views
            [CoreDataImporter saveContext:cdh.importContext];
            
            // STEP 9: turn relationships into faults to break strong reference cycles and save memory
            [cdh.importContext refreshObject:mindfulMinuteTemplate mergeChanges:NO];
            
            NSLog(@"New Item (after refreshObject:mergeChanges:NO): %@", mindfulMinuteTemplate);
        }
        if ([elementName isEqualToString:@"guidedmeditation"]) {
            if(debug==1)NSLog(@"Parsing guidedmeditation element");
            cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
            
            // STEP 2: Choose the unique attribute names
            NSString *guidedMeditationUniqueAttributeValue = [attributeDict valueForKey:@"title"];
            
            // STEP 3: Configure unique attribute for each entity
            NSArray *entities = [NSArray arrayWithObjects: @"GuidedMeditation", nil];
            NSArray *uniqueAttributes = [NSArray arrayWithObjects: @"title", nil];
            NSDictionary *dictionary = [NSDictionary dictionaryWithObjects:uniqueAttributes forKeys:entities];
            
            // STEP 4: Configure attribute values to give the new objects.
            NSArray *guidedMeditationAttributes = @[@"title", @"data"];
            NSURL *dataURL = [[NSBundle mainBundle] URLForResource:attributeDict[@"data"] withExtension:@"mp3"];
            NSError *error = nil;
            NSData *data = [NSData dataWithContentsOfURL:dataURL options:NSDataReadingMappedIfSafe error:&error];
            NSArray *guidedMeditationValues = @[attributeDict[@"title"], data];
            if(error)NSLog(@"%@", error);
            NSDictionary *guidedMeditationAttributeValues = [NSDictionary dictionaryWithObjects:guidedMeditationValues forKeys:guidedMeditationAttributes];
            
            // STEP 5: Create an instance of CoreData Importer
            CoreDataImporter *importer = [[CoreDataImporter alloc] initWithUniqueAttributes:dictionary];
            
            // STEP 6: Create new unique objects
            NSManagedObject *guidedMeditation = [importer insertUniqueObjectForEntity:@"GuidedMeditation" inContext:cdh.importContext withAttributeValues:guidedMeditationAttributeValues uniqueAttributeValue:guidedMeditationUniqueAttributeValue];
            
            // STEP 7: Relate related objects
            
            // STEP 8: Save new objects to the persistent store. This sends the NSManagedObjectContextDidSaveNotification, which triggers a fetch on Core Data table views
            [CoreDataImporter saveContext:cdh.importContext];
            
            // STEP 9: turn relationships into faults to break strong reference cycles and save memory
            [cdh.importContext refreshObject:guidedMeditation mergeChanges:NO];
            
            NSLog(@"New Item (after refreshObject:mergeChanges:NO): %@", guidedMeditation);
        }
    }];
}

@end