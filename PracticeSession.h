//
//  PracticeSession.h
//  WorkWellNW
//
//  Created by Aaron Wells on 2/2/15.
//  Copyright (c) 2015 Aaron Wells. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PracticeSession : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * duration;
@property (nonatomic, retain) NSNumber * guided;

@end
