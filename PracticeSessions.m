//
//  PracticeSessions.m
//  YogaApp
//
//  Created by Aaron Wells on 10/23/14.
//  Copyright (c) 2014 Aaron Wells. All rights reserved.
//

#import "PracticeSessions.h"
#import "PracticeSession.h"
#import "AppDelegate.h"
#import "DateUtils.h"

@interface PracticeSessions ()

@end

static NSArray *_allPracticeSessions = nil;
@implementation PracticeSessions
#define debug 1

+ (NSArray *)allPracticeSessions {
    if (_allPracticeSessions == nil) {
        CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
        NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"PracticeSession"];
        NSError *error = nil;
        _allPracticeSessions = [cdh.context executeFetchRequest:request error:&error];
        if(debug==1)NSLog(@"%@", error);
    }
    
    return _allPracticeSessions;
}

+ (NSArray *)syncPracticeSessions {
    CoreDataHelper *cdh = [(AppDelegate *)[[UIApplication sharedApplication] delegate] cdh];
    NSFetchRequest *request = [NSFetchRequest fetchRequestWithEntityName:@"PracticeSession"];
    NSError *error = nil;
    _allPracticeSessions = [cdh.context executeFetchRequest:request error:&error];
    if(debug==1)NSLog(@"%@", error);
    
    return _allPracticeSessions;
}

+ (NSDictionary*)totalTimePracticed {
    NSTimeInterval timePracticed = 0;
    NSArray *practiceSessions = [self allPracticeSessions];
    for (PracticeSession *ps in practiceSessions)
        timePracticed += [ps.duration doubleValue];
    
    NSDictionary *result = [DateUtils hoursMinutesSeconds:timePracticed];
    return result;
}

//  MARK: I might want to double-check this method
+ (NSInteger)weeksSinceFirstPracticeSession {
    @try {
        NSSortDescriptor *dateAscending = [NSSortDescriptor sortDescriptorWithKey:@"date" ascending:YES];
        NSArray *dateFirstPracticeSessions = [[self allPracticeSessions] sortedArrayUsingDescriptors:[NSArray arrayWithObjects:dateAscending, nil]];
        NSDate *firstSessionDate = [[dateFirstPracticeSessions objectAtIndex:0] date];
        NSDateComponents *week = [[NSCalendar currentCalendar] components:NSCalendarUnitWeekdayOrdinal fromDate:firstSessionDate toDate:[NSDate date] options:0];
        return [week weekdayOrdinal];
    } @catch (NSException *e) {
        if (debug==1) {NSLog(@"Exception: %@ '%@'", e.name, e.reason);}
        return 1;
    }
}

+ (NSInteger)count {
    return _allPracticeSessions.count;
}

@end
